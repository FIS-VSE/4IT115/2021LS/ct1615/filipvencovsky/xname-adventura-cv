package cz.vse.xname._1615_adventura_cv.main;

public interface PredmetPozorovani {
    /**
     * metoda pro přidání pozorovatele do seznamu pozorovatelů
     * @param pozorovatel
     */
    void registrujPozorovatele(Pozorovatel pozorovatel);

    /**
     * metoda pro odebrání pozorovatele ze seznamu pozorovatelů
     * @param pozorovatel
     */
    void odeberPozorovatele(Pozorovatel pozorovatel);

    /**
     * mohla by tu být metoda upozorniPozorovatele, která je ale lepší private
     */
    //private upozorniPozorovatele()
}
