package cz.vse.xname._1615_adventura_cv.main;

public interface Pozorovatel {
    /**
     * metoda, kterou volá předmět pozorování když dojde ke změně
     */
    void update();
}
