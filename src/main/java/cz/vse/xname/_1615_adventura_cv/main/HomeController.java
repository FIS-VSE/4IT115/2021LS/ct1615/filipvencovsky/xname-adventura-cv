package cz.vse.xname._1615_adventura_cv.main;

import cz.vse.xname._1615_adventura_cv.logika.IHra;
import cz.vse.xname._1615_adventura_cv.logika.PrikazJdi;
import cz.vse.xname._1615_adventura_cv.logika.Prostor;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import java.util.HashMap;
import java.util.Map;

public class HomeController implements Pozorovatel {

    @FXML private ImageView hrac;
    @FXML private ListView<Prostor> seznamVychodu;
    @FXML private Button odesli;
    @FXML private TextArea vystup;
    @FXML private TextField vstup;
    private Map<String, Point2D> souradniceProstoru = new HashMap<>();
    private Map<String, Image> obrazkyProstoru = new HashMap<>();

    private IHra hra;

    public void setHra(IHra hra) {
      this.hra = hra;
      vystup.appendText(hra.vratUvitani()+"\n\n");
      hra.getHerniPlan().registrujPozorovatele(this);
      seznamVychodu.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());

      obrazkyProstoru.put("les", new Image(getClass().getResourceAsStream("/hrac.png")));
      obrazkyProstoru.put("jeskyně", new Image(getClass().getResourceAsStream("/hrac.png")));

      seznamVychodu.setCellFactory(new Callback<ListView<Prostor>, ListCell<Prostor>>() {
          @Override
          public ListCell<Prostor> call(ListView<Prostor> prostorListView) {
              return new ListCell<Prostor>() {
                  protected void updateItem(Prostor prostor, boolean empty) {
                      super.updateItem(prostor, empty);
                      if (prostor != null) {
                          setText(prostor.getNazev());
                          ImageView pohled = new ImageView(obrazkyProstoru.get(prostor.getNazev()));
                          pohled.setPreserveRatio(true);
                          pohled.setFitHeight(70);
                          setGraphic(pohled);
                      } else {
                          setText("");
                          setGraphic(new ImageView());
                      }
                  }
              };
          }
      });

      souradniceProstoru.put("domeček", new Point2D(26,97));
      souradniceProstoru.put("les", new Point2D(111,51));
      souradniceProstoru.put("hluboký_les", new Point2D(194,97));
      souradniceProstoru.put("chaloupka", new Point2D(277,51));
      souradniceProstoru.put("jeskyně", new Point2D(194,196));
    }

    public void initialize() {
        vystup.setEditable(false);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vstup.requestFocus();
            }
        });
    }

    public void odeslanVstup(ActionEvent actionEvent) {
        String prikaz = vstup.getText();
        zpracujPrikaz(prikaz);
        vstup.clear();
    }

    private void zpracujPrikaz(String prikaz) {

        String vysledek = hra.zpracujPrikaz(prikaz);

        vystup.appendText("> "+prikaz+"\n");
        vystup.appendText(vysledek+"\n\n");


        if(hra.konecHry()) {
            vstup.setDisable(true);
            odesli.setDisable(true);
            seznamVychodu.setDisable(true);
            vystup.appendText(hra.vratEpilog());
        }

    }

    @Override
    public void update() {
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();

        seznamVychodu.getItems().clear();
        seznamVychodu.getItems().addAll(aktualniProstor.getVychody());

        Point2D souradniceAktualnihoProstoru = souradniceProstoru.get(aktualniProstor.getNazev());

        hrac.setLayoutX(souradniceAktualnihoProstoru.getX());
        hrac.setLayoutY(souradniceAktualnihoProstoru.getY());
    }

    public void vybranyVychod(MouseEvent mouseEvent) {
        Prostor vybranyVychod = seznamVychodu.getSelectionModel().getSelectedItem();
        if(vybranyVychod==null) return;
        zpracujPrikaz(PrikazJdi.NAZEV+" "+vybranyVychod);
    }
}

